<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit Privileges</title>
</head>
<body>
	<form action="add" method="get">
		<label>User Name: <input type="text" id="username" name="username"/></label><br/>
		<label>Add "Premium" privilege <input type="radio" name="privilege" id="addprivilege"/></label>
		<label>Remove "Premium" privilege <input type="radio" name="privilege" id="removeprivilege"/></label>
	</form>
</body>
</html>