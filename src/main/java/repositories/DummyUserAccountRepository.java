package repositories;

import java.util.ArrayList;
import java.util.List;

import domain.UserAccount;

public class DummyUserAccountRepository implements UserAccountRepository {
	private static List<UserAccount> db = new ArrayList<UserAccount>();

	@Override
	public UserAccount getAccountByEmail(String email) {
		for(UserAccount account: db) {
			if(account.getEmail().equalsIgnoreCase(email)) {
				return account;
			}
		}
		return null;
	}

	@Override
	public void add(UserAccount account) {
		db.add(account);
	}

	@Override
	public UserAccount getAccountByUserNameAndPassword(String username, String password) {
		for(UserAccount account: db) {
			if(account.getEmail().equalsIgnoreCase(username)) {
				if(account.getPassword().equals(password)) {
					return account;
				}
			}
		}
		return null;
	}

	@Override
	public List<UserAccount> getAccounts() {
		return db;
	}

}
