package repositories;

import java.util.List;

import domain.UserAccount;

public interface UserAccountRepository {
	UserAccount getAccountByEmail(String email);
	void add(UserAccount account);
	UserAccount getAccountByUserNameAndPassword(String username, String password);
	List<UserAccount> getAccounts();
}
