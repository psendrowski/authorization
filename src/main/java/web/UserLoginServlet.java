package web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.UserAccount;
import repositories.DummyUserAccountRepository;
import repositories.UserAccountRepository;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet("/UserLoginServlet")
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
        String password = request.getParameter("password"); 
        
        HttpSession session = request.getSession();
        UserAccountRepository repository = new DummyUserAccountRepository();
        
        UserAccount account = repository.getAccountByUserNameAndPassword(username, password);
        
        if(account != null) {
        	session.setAttribute("username", account.getUserName());
        	session.setAttribute("password", account.getPassword());
        	session.setAttribute("email", account.getEmail());
        	
        	response.sendRedirect("/UserAccount");
        } else {
        	response.sendRedirect("login-error.jsp");
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
