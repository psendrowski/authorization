package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.UserAccount;
import repositories.DummyUserAccountRepository;
import repositories.UserAccountRepository;

/**
 * Servlet implementation class UserAccountsListServlet
 */
@WebServlet("/UserAccountsList")
public class UserAccountsListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAccountsListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserAccountRepository repository = new DummyUserAccountRepository();
		List<UserAccount> accountList = repository.getAccounts();
		
		PrintWriter out = response.getWriter();
		for(UserAccount account: accountList) {
			out.println("User Name: " + account.getUserName());
	        out.println("E-mail: " + account.getEmail());
	        out.println();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
