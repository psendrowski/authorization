package web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.UserAccount;
import repositories.DummyUserAccountRepository;
import repositories.UserAccountRepository;

/**
 * Servlet implementation class AddAccountServlet
 */
@WebServlet("/add")
public class AddAccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	/*
    public AddAccountServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    */

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		HttpSession session = request.getSession();
		
		/*
		if(session.getAttribute("conf") != null) {
			response.getWriter().println("You can not re-register.");
			return;
		}
		*/
		
		UserAccount account = retriveUserAccountFromRequest(request);
		UserAccountRepository repository = new DummyUserAccountRepository();
		
		session.setAttribute("conf", account);
		
		repository.add(account);
		response.sendRedirect("registered.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/*
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	*/
	
	private UserAccount retriveUserAccountFromRequest(HttpServletRequest request) {
		UserAccount result = new UserAccount();
		result.setUserName(request.getParameter("username"));
		result.setEmail(request.getParameter("email"));
		result.setPassword(request.getParameter("password"));
		return result;
	}
}
